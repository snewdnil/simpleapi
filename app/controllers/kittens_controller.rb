class KittensController < ApplicationController
  def index
    @kitten = Kitten.all

    respond_to do |format|
      format.html
      format.json { render :json => @kitten }
    end
  end

  def show
    @kitten = Kitten.find(params[:id])
    respond_to do |format|
      format.html { render "kittens/index" }
      format.json { render :json => @kitten }
    end
  end

  def new
    @kitten = Kitten.new
  end

  def create
    @kitten = Kitten.new(kitten_params)

    if @kitten.save
      flash[:success] = 'Thank you for register your kitten!'
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
    @kitten = Kitten.find(params[:id]) || nil

    if @kitten == nil
      redirect_to root_path
    end
  end

  def update
    @kitten = Kitten.find(params[:kitten][:id]) || nil
    if @kitten == nil
      render 'new'
    else
      flash[:edit] = "Thank you for maintain this website updated!"
      @kitten.update(kitten_params)
      redirect_to root_path
    end
  end

  def delete
    @kitten = Kitten.find(params[:kitten_id]) || nil
    if @kitten != nil
      @kitten.delete
    end
    redirect_to root_path
  end

  private
  def kitten_params
    params.require(:kitten).permit(:name, :age, :cuteness, :softness)
  end
end
