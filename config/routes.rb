Rails.application.routes.draw do
  resource :kittens

  delete '/' => 'kittens#delete', :as => :delete
  patch '/edit' => 'kittens#update', :as => :update
  post '/edit' => 'kittens#edit', :as => :edit
  get '/:id' => 'kittens#show'

  root 'kittens#index'
end
